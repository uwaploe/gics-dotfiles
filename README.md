# GICS Dotfiles

This repository contains various configuration files for the system
operator account (sysop) on the GICS computer system. Clone this
repository to the directory `~/dotfiles` then run:

``` shellsession
cd ~/dotfiles
./install.sh
```
