#!/usr/bin/env bash

DIRS=("bash" "tmux" "git" "emacs")

for d in "${DIRS[@]}"; do
    stow $d
done
