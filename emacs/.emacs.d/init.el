;; This is intended for manually installed libraries
(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'load-path "~/.emacs.d/site-lisp/")

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'mfk-funcs)

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)

;; No toolbar or scrollbar
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; Start with the *scratch* buffer
(setq inhibit-startup-screen t)

;; Tabs are evil
(setq-default indent-tabs-mode nil)

;; Keep buffers in sync with files
(global-auto-revert-mode t)

;; Ignore case when completing filenames
(setq read-file-name-completion-ignore-case t)

;; Answer y/n instead of yes/no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Autofill
(setq-default fill-column 74)
(setq-default default-tab-width 4)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Better unique names for buffers.
(setq uniquify-buffer-name-style 'forward)

;; Do not create backup files
(setq backup-inhibited 1)

;; Always (usually) delete trailing whitespace
(defvar mfk/nuke-whitespace t)
(add-hook 'before-save-hook
          (lambda ()
            (when mfk/nuke-whitespace (delete-trailing-whitespace))))

(use-package exec-path-from-shell
  :ensure t
  :config
  (progn
    (setq exec-path-from-shell-check-startup-files 'nil)))

(mfk/on-gui
 (exec-path-from-shell-initialize)
 (exec-path-from-shell-copy-env "GOPATH")
 (exec-path-from-shell-copy-env "PYTHONPATH"))

;; Smarter comment
(use-package smart-comment
  :ensure t
  :bind ("M-;" . smart-comment))

(use-package crux
  :ensure t
  :bind (("C-a" . crux-move-beginning-of-line)
         ("C-c o" . crux-open-with)))

(use-package nix-mode
  :ensure t)

(use-package auto-complete
  :ensure t
  :commands auto-complete-mode
  :init
  (progn
    (auto-complete-mode t))
  :bind (("M-n" . ac-next)
         ("M-p" . ac-previous))
  :config
  (progn
    (use-package auto-complete-config)

    (ac-set-trigger-key "TAB")
    (ac-config-default)

    (setq ac-delay 0.02)
    (setq ac-use-menu-map t)
    (setq ac-menu-height 50)
    (setq ac-use-quick-help nil)
    (setq ac-comphist-file  "~/.emacs.d/ac-comphist.dat")
    (setq ac-ignore-case nil)
    (setq ac-dwim  t)
    (setq ac-fuzzy-enable t)

    (use-package ac-dabbrev
      :ensure t
      :config
      (progn
        (add-to-list 'ac-sources 'ac-source-dabbrev)))

    (setq ac-modes '(js3-mode
                     emacs-lisp-mode
                     lisp-mode
                     lisp-interaction-mode
                     slime-repl-mode
                     c-mode
                     cc-mode
                     c++-mode
                     go-mode
                     java-mode
                     perl-mode
                     cperl-mode
                     python-mode
                     makefile-mode
                     sh-mode))))

(use-package go-autocomplete
  :ensure t)
;;
;;** CC-mode
;;
(autoload 'c++-mode  "cc-mode" "C++ Editing Mode" t)
(autoload 'c-mode    "cc-mode" "C Editing Mode" t)
(autoload 'java-mode "cc-mode" "Java Editing Mode" t)
(setq auto-mode-alist
      (append '(("\\.C$"  . c++-mode)
		("\\.cc$" . c++-mode)
		("\\.c$"  . c-mode)
		("\\.h$"  . c-mode)
		("\\.h\\.in$"  . c-mode)
		("\\.java$" . java-mode)
		) auto-mode-alist))

(defconst my-c-style
  '((c-basic-offset . 4)
    (c-tab-always-indent . nil)
    (c-toggle-auto-state . 1)
    (c-offsets-alist . ((statement-cont . *)
			(statement-case-open . -)
			(inline-open . -)
			(block-open . -)
			(brace-list-open . -)
			(substatement . +)
			(substatement-open . 0)
			(knr-argdecl-intro . 0)
			(case-label . +)
			(access-label . 0)
			(label . 0)))
    (c-hanging-braces-alist . ((block-close . c-snug-do-while)
			       (defun-open . (before after))
			       (substatement-open . (before after))
			       (brace-list-open . ())
			       (brace-list-intro . ())
			       (brace-list-entry . ())
			       (brace-list-close . ())
			       (statement-case-open . (after))
			       (inline-open . (after))
			       (class-open after))))
  "My C source formatting style")

(c-add-style "mystyle" my-c-style)

(defun mfk/c-mode-hook ()
  (add-to-list 'ac-sources 'ac-source-c-headers)
  (c-set-style "mystyle")
  (setq c-double-slash-is-comments-p t)
  (mfk/untabify-buffer-hook))


(add-hook 'c-mode-common-hook 'mfk/c-mode-hook)

;; Makefile
(add-hook 'makefile-mode-hook
          (lambda()
            (setq show-trailing-whitespace t)))

;; Shell
(defun set-scroll-conservatively ()
  "Prevent jump-scrolling on newlines in shell buffers."
  (set (make-local-variable 'scroll-conservatively) 10))

(add-hook 'shell-mode-hook 'set-scroll-conservatively)
(add-hook 'shell-mode-hook
          (function (lambda ()
                      (local-set-key (quote [f1]) (quote send-invisible)))))
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(add-hook 'comint-output-filter-functions 'comint-truncate-buffer)
(add-hook 'after-save-hook 'make-buffer-file-executable-if-script-p)

;;
;; GO
;;
(add-hook 'go-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'gofmt-before-save)
            (setq gofmt-command "goimports")
            (yas-minor-mode)
            (local-set-key (kbd "M-.") 'godef-jump)         ; Go to definition
            (local-set-key (kbd "M-*") 'pop-tag-mark)       ; Return from whence you came
            (local-set-key (kbd "M-p") 'compile)            ; Invoke compiler
            (local-set-key (kbd "M-P") 'recompile)          ; Redo most recent compile cmd
            (local-set-key (kbd "M-]") 'next-error)         ; Go to next error (or msg)
            (local-set-key (kbd "M-[") 'previous-error)     ; Go to previous error or msg
            (auto-complete-mode 1)
            (setq tab-width 4)
            (setq indent-tabs-mode 1)))

;; Ensure the go specific autocomplete is active in go-mode.
(with-eval-after-load 'go-mode
  (require 'go-autocomplete))

;; keep customize settings in their own file
(setq custom-file "~/.emacs.d/custom.el")
(when (file-exists-p custom-file)
  (load custom-file))
