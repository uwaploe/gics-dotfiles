;;
;;* Utility functions
;;
(defun ts ()
  "Add timestamp at point"
  (interactive)
  (insert (format-time-string "*%Y-%m-%d %H:%M:%S%z" (current-time))))

(defun my-ip-address (interface)
  "Return the IP address of the eth0 interface as a string"
  (let ((buf (get-buffer-create "*ifconfig-output*"))
	(addr "")
	(astart nil))
    (save-excursion
      (set-buffer buf)
      (erase-buffer)
      (call-process "/sbin/ifconfig" nil t nil interface)
      (goto-char (point-min))
      (search-forward "inet addr:" nil t)
      (setq astart (point))
      (if (looking-at "[0-9]+\.")
	  (setq addr (buffer-substring astart (search-forward " ")))))
    (kill-buffer buf)
    addr))

(defun my-str-join (strlist sep)
  "Join the components of STRLIST into a single string with each component
separated by the string SEP"
  (if (null (cdr strlist))
      (car strlist)
    (concat (car strlist) sep (my-str-join (cdr strlist) sep))))

(defun my-hostname ()
  (car (split-string system-name "[\.]")))

(defun my-dnsdomain ()
  (my-str-join (cdr (split-string system-name "[\.]")) "."))

(defun at-home ()
  (let ((m (string-match "192\.168\.42\.[0-9]+" (my-ip-address "eth0"))))
    (or (not (string= (my-ip-address "ppp0") ""))
	(and m (= 0 m)))))

(defun make-buffer-file-executable-if-script-p ()
  "Make file executable according to umask if not already executable.
If file already has any execute bits set at all, do not change existing
file modes."
  (and (save-excursion
	 (save-restriction
	   (widen)
	   (goto-char (point-min))
	   (save-match-data
	     (looking-at "^#!"))))
       (let* ((current-mode (file-modes (buffer-file-name)))
	      (add-mode (logand ?\111 (default-file-modes))))
	 (or (/= (logand ?\111 current-mode) 0)
	     (zerop add-mode)
	     (set-file-modes (buffer-file-name)
			     (logior current-mode add-mode))))))

(defun undos ()
    "Un-dosify the current buffer by removing carriage returns and ^Zs"
    (interactive)
    (shell-command-on-region (point-min) (point-max) "tr -d \"\\015\\032\""
      nil t)
)

;; http://tsdh.wordpress.com/2008/08/20/re-open-read-only-files-as-root-automagically/
(defun th-find-file-sudo (file)
  "Opens FILE with root privileges."
  (interactive "F")
  (set-buffer (find-file (concat "/sudo::" file)))
  (rename-buffer (concat "sudo::" (buffer-name))))

(defun th-find-file-sudo-maybe ()
  "Re-finds the current file as root if it's read-only after
querying the user."
  (interactive)
  (let ((file (buffer-file-name)))
    (and (not (file-writable-p file))
         (y-or-n-p "File is read-only.  Open it as root? ")
         (progn
           (kill-buffer (current-buffer))
           (th-find-file-sudo file)))))

(defun mfk/untabify-buffer ()
  "Remove tabs from entrire buffer"
  (interactive)
  (save-excursion
    (untabify (point-min) (point-max))))

(defun mfk/clean-file ()
  "Remove tabs and trailing whitespace from the entire buffer"
  (interactive)
  (mfk/untabify-buffer)
  (delete-trailing-whitespace))

(defun mfk/untabify-buffer-hook ()
  "Adds a buffer-local untabify on save hook"
  (interactive)
  (add-hook
   'after-save-hook
   (lambda () (when mfk/nuke-whitespace (mfk/untabify-buffer)))
   nil
   'true))

(defun mfk/use-venv (name)
  "Switch to a different Python virtual environment"
  (interactive "sVenv name: ")
  (pyvenv-workon name)
  (elpy-rpc-restart))

;; From http://oremacs.com/
(defun terminal ()
  "Switch to terminal. Launch if nonexistent."
  (interactive)
  (if (get-buffer "*ansi-term*")
      (switch-to-buffer "*ansi-term*")
    (ansi-term "/bin/bash"))
  (get-buffer-process "*ansi-term*"))

(defalias 'tt 'terminal)

(defun dired-open-term ()
  "Open an `ansi-term' that corresponds to current directory."
  (interactive)
  (let ((current-dir (dired-current-directory)))
    (term-send-string
     (terminal)
     (if (file-remote-p current-dir)
         (let ((v (tramp-dissect-file-name current-dir t)))
           (format "ssh %s@%s\n"
                   (aref v 1) (aref v 2)))
       (format "cd '%s'\n" current-dir)))))

(defun youtube-dl ()
  (interactive)
  (let* ((str (current-kill 0))
         (default-directory "~/Downloads/Videos")
         (proc (get-buffer-process (ansi-term "/bin/bash"))))
    (term-send-string
     proc
     (concat "cd ~/Downloads/Videos && youtube-dl " str "\n"))))

(defun mfk/get-url ()
  (shell-command-to-string
   "osascript -e 'tell application \"Safari\" to return URL of document 1'"))

;; From http://irreal.org/blog/?p=2895
(defun mfk/insert-link (link)
  "Retrieve URL from current Safari page and prompt for description.
Insert an Org link at point."
  (interactive "sLink Description: ")
  (let ((result
         (mfk/get-url)))
    (insert (format "[[%s][%s]]" (org-trim result) link))))


(defun mfk/insert-url ()
  (interactive)
  (insert (mfk/get-url)))

(defun mfk/insert-timestamp ()
  "Produces and inserts a full ISO 8601 format timestamp."
  (interactive)
  (insert (format-time-string "%Y-%m-%dT%T")))

(defun mfk/insert-date ()
  "Produces and inserts the current date."
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))

(defmacro mfk/on-linux (statement &rest statements)
  "Evaluate the enclosed body only when run on GNU/Linux."
  `(when (eq system-type 'gnu/linux)
     ,statement
     ,@statements))

(defmacro mfk/on-osx (statement &rest statements)
  "Evaluate the enclosed body only when run on OSX."
  `(when (eq system-type 'darwin)
     ,statement
     ,@statements))

(defmacro mfk/on-gui (statement &rest statements)
  "Evaluate the enclosed body only when run on GUI."
  `(when (display-graphic-p)
     ,statement
     ,@statements))

(defmacro mfk/not-on-gui (statement &rest statements)
  "Evaluate the enclosed body only when run on GUI."
  `(when (not (display-graphic-p))
     ,statement
     ,@statements))

;; From: https://github.com/magnars/.emacs.d/blob/master/defuns/lisp-defuns.el
(defun mfk/eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid expression")
           (insert (current-kill 0)))))

(defun mfk/enable-abbrev-mode ()
  (interactive)
  (abbrev-mode t))

;; Generate a list of header-file directory directives for an MLF2
;; project. This requires Projectile.
(defun mfk/mlf2-header-dirs ()
  (mapcar (lambda (p)
            (concat (if (file-name-absolute-p p)
                        ""
                      (projectile-project-root)) p))
          (list (expand-file-name "~/projects/MLF2-git/mlf2-lib/libmlf2")
                (expand-file-name "~/projects/MLF2-git/tt8lib")
                "mission"
                "ballast"
                "/usr/local/mlf2/include"
                "")))

;; Convert decimal degrees to degrees and decimal minutes
(defun mfk/deg-to-dmm (deg)
  (let ((hms (mapcar 'string-to-number
                     (split-string
                      (calc-eval
                       (format "hms(%f)" deg)) "[@'\"]"))))
    (list (nth 0 hms)
          (string-to-number (calc-eval
                             (format "%d+%f/60" (nth 1 hms)
                                     (nth 2 hms)))))))


;; https://hbfs.wordpress.com/2016/04/19/respace/
(defun mfk/ws-respace()
  "Collapse whitespace then reindent"
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (replace-regexp "[[:space:]]+" " ")
    (indent-region (region-beginning) (region-end) nil)))

(defun mfk/camelCase-to_underscores (start end)
  "Convert any string matching something like aBc to a_bc"
  (interactive "r")
  (save-restriction
    (narrow-to-region start end)
    (goto-char 1)
    (let ((case-fold-search nil))
      (while (search-forward-regexp "\\([a-z]\\)\\([A-Z]\\)\\([a-z]\\)" nil t)
        (replace-match (concat (downcase (match-string 1))
                               "_"
                               (downcase (match-string 2))
                               (match-string 3))
                       t nil)))))
  (provide 'mfk-funcs)
